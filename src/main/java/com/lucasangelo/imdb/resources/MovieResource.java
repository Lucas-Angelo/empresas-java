package com.lucasangelo.imdb.resources;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

import com.lucasangelo.imdb.domain.Movie;
import com.lucasangelo.imdb.dto.MovieDTO;
import com.lucasangelo.imdb.dto.MovieNewDTO;
import com.lucasangelo.imdb.resources.utils.URL;
import com.lucasangelo.imdb.services.MovieService;

@RestController
@RequestMapping(value="/movies")
public class MovieResource {
	
	@Autowired
	private MovieService service;
	
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody MovieNewDTO objDto) {
        Movie obj = service.fromDTO(objDto);
        obj = service.create(obj);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}").buildAndExpand(obj.getId()).toUri(); // Return new Movie URL
        return ResponseEntity.created(uri).build();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<MovieDTO> findById(@PathVariable Integer id) {
        Movie obj = service.find(id);
        MovieDTO MovieDTO = new MovieDTO(obj);
        return ResponseEntity.ok().body(MovieDTO);
    }
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<Page<MovieDTO>> findPage(
			@RequestParam(value="name", defaultValue="") String name, 
			@RequestParam(value="genres", defaultValue="") String genres, 
			@RequestParam(value="actors", defaultValue="") String actors, 
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="rating") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {
		
		String nameDecoded; // To remove % encoded
		List<Integer> genreIds; // Get genre ids
		List<Integer> actorsIds; // Get genre ids

		Page<Movie> list;
		if (genres.length()>0 && actors.length()>0 && name.length()>0) {
			nameDecoded = URL.decodeParam(name);
			genreIds = URL.decodeIntList(genres);
			actorsIds = URL.decodeIntList(actors);
			list = service.findDistinctByNameContainingAndGenresInAndActorsIn(nameDecoded, genreIds, actorsIds, page, linesPerPage, orderBy, direction);
		}
		else if (genres.length()>0 && name.length()>0) {
			nameDecoded = URL.decodeParam(name);
			genreIds = URL.decodeIntList(genres);
			list = service.findDistinctByNameContainingAndGenresIn(nameDecoded, genreIds, page, linesPerPage, orderBy, direction);
		}
		else if (actors.length()>0 && name.length()>0) {
			nameDecoded = URL.decodeParam(name);
			actorsIds = URL.decodeIntList(actors);
			list = service.findDistinctByNameContainingAndActorsIn(nameDecoded, actorsIds, page, linesPerPage, orderBy, direction);
		}
		else {
			list = service.findAll(page, linesPerPage, orderBy, direction);
		}
		
		Page<MovieDTO> listDto = list.map(obj -> new MovieDTO(obj));
		return ResponseEntity.ok().body(listDto);
	}

}
