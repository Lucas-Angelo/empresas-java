package com.lucasangelo.imdb.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import com.lucasangelo.imdb.domain.Vote;
import com.lucasangelo.imdb.dto.VoteDTO;
import com.lucasangelo.imdb.dto.VoteNewDTO;
import com.lucasangelo.imdb.services.VoteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping(value = "/votes")
public class VoteResource {

    @Autowired
    private VoteService service;

    @PreAuthorize("hasAnyRole('STANDARD')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody VoteNewDTO objDto) {
        Vote obj = service.fromDTO(objDto);
        obj = service.create(obj);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}").buildAndExpand(obj.getId()).toUri(); // Return new Vote URL
        return ResponseEntity.created(uri).build();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<VoteDTO> findById(@PathVariable Integer id) {
        Vote obj = service.find(id);
        VoteDTO VoteDTO = new VoteDTO(obj);
        return ResponseEntity.ok().body(VoteDTO);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<VoteDTO>> findAll() {
        List<Vote> list = service.findAll();
        List<VoteDTO> listDTO = list.stream().map(obj -> new VoteDTO(obj)).collect(Collectors.toList());
        return ResponseEntity.ok().body(listDTO);
    }
}
