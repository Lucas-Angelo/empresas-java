package com.lucasangelo.imdb.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Vote implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;

	@JsonIgnore
    @ManyToOne
	@JoinColumn(name="movie_id")
	private Movie movie;
	
	private Double rate;
	
	public Vote() {
	}

	public Vote(Integer id, Double rate, User user, Movie movie) {
		super();
		this.id = id;
		if (rate>4.0d)
			this.rate = 4.0d;
		else if (rate<0.0d)
			this.rate = 4.0d;
		else
			this.rate = rate;
		this.user = user;
		this.movie = movie;
	}
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

    public Double getRate() {
        return this.rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Movie getMovie() {
        return this.movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vote other = (Vote) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Número do voto: ");
		builder.append(getId());
		builder.append(", Usuário: ");
		builder.append(getUser().getName());
		builder.append("Nota: ");
		builder.append(getRate());
		return builder.toString();
	}
}
