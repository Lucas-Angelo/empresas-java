package com.lucasangelo.imdb.domain.enums;

public enum Profile {

    ADMIN(0, "ROLE_ADMIN"),
    STANDARD(1, "ROLE_STANDARD");
    
    private int code;
    private String description;

    private Profile(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
    
    public Integer getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

	public static Profile toEnum(Integer code) {
		if (code == null) {
			return null;
		}
		
		for (Profile x : Profile.values()) {
			if (code.equals(x.getCode())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Código inválido: " + code);
	}
}
