package com.lucasangelo.imdb.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Movie implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String director;

    @ManyToMany // One movie can have multiple genres and one genre can be multiple movies.
    @JoinTable (
        name = "MOVIE_GENRE", /* Name of the table */
        joinColumns = @JoinColumn(name = "movie_id"), /* Movie FK */
        inverseJoinColumns = @JoinColumn(name = "genre_id") /* Genre FK */
    )
    private List<Genre> genres = new ArrayList<>();

    @ManyToMany // One movie can have multiple actors and one actor can be multiple movies.
    @JoinTable (
        name = "MOVIE_ACTOR", /* Name of the table */
        joinColumns = @JoinColumn(name = "movie_id"), /* Movie FK */
        inverseJoinColumns = @JoinColumn(name = "actor_id") /* Actor FK */
    )
    private List<Actor> actors = new ArrayList<>();

    @OneToMany(mappedBy = "movie")
    private List<Vote> votes = new LinkedList<>();

    @JsonProperty(access = Access.READ_ONLY) // Not to show this field in response
    private Double rating;
    
    public Movie() {
    
    }

    public Movie(Integer id, String name, String director, List<Genre> genres, List<Actor> actors) {
        this.id = id;
        this.name = name;
        this.director = director;
        this.genres = genres;
        this.actors = actors;
        this.rating = getRating();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirector() {
        return this.director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public List<Genre> getGenres() {
        return this.genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<Actor> getActors() {
        return this.actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public List<Vote> getVotes() {
        return this.votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

    public Double getRating() {
        if (this.votes.isEmpty())
            return 0.0d;
        else {
            Double sum = 0.0d;
            for (Vote vote : this.votes) {
                sum += vote.getRate();
            }
            this.rating = sum/this.votes.size();
            return this.rating;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Movie))
            return false;
        Movie other = (Movie) obj;
        if (id == null)
            if (other.id != null)
                return false;
        else if (!id.equals(other.id))
            return false;
        return true;
    }

}
