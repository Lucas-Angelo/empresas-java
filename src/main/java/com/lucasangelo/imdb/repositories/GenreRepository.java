package com.lucasangelo.imdb.repositories;

import com.lucasangelo.imdb.domain.Genre;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Integer> {
    
}
