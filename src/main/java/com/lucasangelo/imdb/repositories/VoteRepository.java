package com.lucasangelo.imdb.repositories;

import com.lucasangelo.imdb.domain.Vote;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Integer> {
    
}
