package com.lucasangelo.imdb.repositories;

import com.lucasangelo.imdb.domain.Actor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {
    
}
