package com.lucasangelo.imdb.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lucasangelo.imdb.domain.Actor;
import com.lucasangelo.imdb.domain.Genre;
import com.lucasangelo.imdb.domain.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {

	// Spring make JPQL query with method signature == findDistinctByNameContainingAndGenresInAndActorsIn()
	@Transactional(readOnly=true)
	Page<Movie> findDistinctByNameContainingAndGenresInAndActorsIn(String name, List<Genre> genres, List<Actor> actors, Pageable pageRequest);

	@Transactional(readOnly=true)
	Page<Movie> findDistinctByNameContainingAndGenresIn(String name, List<Genre> genres, Pageable pageRequest);

	@Transactional(readOnly=true)
	Page<Movie> findDistinctByNameContainingAndActorsIn(String name, List<Actor> actors, Pageable pageRequest);

}
