package com.lucasangelo.imdb.services.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.lucasangelo.imdb.domain.User;
import com.lucasangelo.imdb.dto.UserNewDTO;
import com.lucasangelo.imdb.repositories.UserRepository;
import com.lucasangelo.imdb.resources.exception.FieldMessage;

public class UserCreateValidator implements ConstraintValidator<UserCreate, UserNewDTO> {

	@Autowired
	private UserRepository repo;
	
	@Override
	public void initialize(UserCreate ann) {
	}

	@Override
	public boolean isValid(UserNewDTO objDto, ConstraintValidatorContext context) {
		
		List<FieldMessage> list = new ArrayList<>();

		User aux = repo.findByEmail(objDto.getEmail());
		if (aux != null) {
			list.add(new FieldMessage("email", "Email já cadastrado"));
		}
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}
