package com.lucasangelo.imdb.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.lucasangelo.imdb.domain.Actor;
import com.lucasangelo.imdb.domain.Genre;
import com.lucasangelo.imdb.domain.Movie;
import com.lucasangelo.imdb.dto.MovieNewDTO;
import com.lucasangelo.imdb.repositories.ActorRepository;
import com.lucasangelo.imdb.repositories.GenreRepository;
import com.lucasangelo.imdb.repositories.MovieRepository;
import com.lucasangelo.imdb.resources.utils.URL;
import com.lucasangelo.imdb.services.exceptions.DataIntegrityException;
import com.lucasangelo.imdb.services.exceptions.ObjectNotFoundException;

@Service
public class MovieService {
	
	@Autowired
	private MovieRepository repository;
	
	@Autowired
	private GenreRepository genreRepository;

	@Autowired
	private ActorRepository actorRepository;
	
    public Movie create(Movie obj) {
        obj.setId(null); // To protect if Id isn't null...
        return repository.save(obj);
    }

    public void delete(Integer id){
        find(id);
        try {
            repository.deleteById(id);
        } catch (DataIntegrityException e) {
            throw new DataIntegrityException("Não é possível excluir este usuário.");
        }
    }

    public Movie find(Integer id) {
        Optional<Movie> obj = repository.findById(id); 
        return obj.orElseThrow(() -> new ObjectNotFoundException(
            "Objeto não encontrado! Id: " + id + ", Tipo: " + Movie.class.getName())
        ); 
    }

	public Page<Movie> findAll(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repository.
        findAll(pageRequest);
	}

	public Movie fromDTO(MovieNewDTO objDto) { // some bug here
		List<Integer> genresIds = URL.decodeIntList(objDto.getGenres());
		List<Genre> genres = genreRepository.findAllById(genresIds);

		List<Integer> actorsIds = URL.decodeIntList(objDto.getActors());
		List<Actor> actors = actorRepository.findAllById(actorsIds);

        return new Movie(null, objDto.getName(), objDto.getDirector(), genres, actors);
    }

	public Page<Movie> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repository.
        findAll(pageRequest);
	}

	public Page<Movie> findDistinctByNameContainingAndGenresInAndActorsIn(String name, List<Integer> genreIds, List<Integer> actorIds, Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		List<Genre> genres = genreRepository.findAllById(genreIds);
		List<Actor> actors = actorRepository.findAllById(actorIds);
		return repository.findDistinctByNameContainingAndGenresInAndActorsIn(name, genres, actors, pageRequest); // or repository.search()
	}

	public Page<Movie> findDistinctByNameContainingAndGenresIn(String name, List<Integer> genreIds, Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		List<Genre> genres = genreRepository.findAllById(genreIds);
		return repository.findDistinctByNameContainingAndGenresIn(name, genres, pageRequest); // or repository.search()
	}

	public Page<Movie> findDistinctByNameContainingAndActorsIn(String name, List<Integer> actorIds, Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		List<Actor> actors = actorRepository.findAllById(actorIds);
		return repository.findDistinctByNameContainingAndActorsIn(name, actors, pageRequest); // or repository.search()
	}
}
