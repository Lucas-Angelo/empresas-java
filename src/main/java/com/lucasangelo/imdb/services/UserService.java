package com.lucasangelo.imdb.services;

import java.util.List;
import java.util.Optional;

import com.lucasangelo.imdb.domain.User;
import com.lucasangelo.imdb.domain.enums.Profile;
import com.lucasangelo.imdb.dto.UserDTO;
import com.lucasangelo.imdb.dto.UserNewDTO;
import com.lucasangelo.imdb.repositories.UserRepository;
import com.lucasangelo.imdb.security.UserSS;
import com.lucasangelo.imdb.services.exceptions.AuthorizationException;
import com.lucasangelo.imdb.services.exceptions.DataIntegrityException;
import com.lucasangelo.imdb.services.exceptions.ObjectNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRepository repository;

    public User create(User obj) {
        obj.setId(null); // To protect if Id isn't null...
        obj.setPassword(bCryptPasswordEncoder.encode(obj.getPassword()));
        return repository.save(obj);
    }

    public User update(User obj) {
        User newObj = find(obj.getId());
		updateData(newObj, obj);
		return repository.save(newObj);
	}

    public void delete(Integer id){
        find(id);
        try {
            repository.deleteById(id);
        } catch (DataIntegrityException e) {
            throw new DataIntegrityException("Não é possível excluir este usuário.");
        }
    }

    public User find(Integer id) {
        UserSS user = authenticated();

        if(user==null || !user.hasRole(Profile.ADMIN) && !id.equals(user.getId()))
            throw new AuthorizationException("Acesso negado");


        Optional<User> obj = repository.findById(id); 
        return obj.orElseThrow(() -> new ObjectNotFoundException(
            "Objeto não encontrado! Id: " + id + ", Tipo: " + User.class.getName())
        ); 
    }

    public List<User> findAll() {
        return repository.findAll();
    }

    public User fromDTO(UserDTO objDto) {
        return new User(objDto.getId(), objDto.getName(), objDto.getEmail(), objDto.getPassword());
    }

    public User fromDTO(UserNewDTO objDto) {
        return new User(null, objDto.getName(), objDto.getEmail(), objDto.getPassword());
    }

	public Page<User> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repository.
        findAll(pageRequest);
	}

    private void updateData(User newObj, User obj) {
        newObj.setName(obj.getName());
        newObj.setEmail(obj.getEmail());
        newObj.setPassword(obj.getPassword());
    }

    public static UserSS authenticated() {
        try {
            return (UserSS) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            return null;
        }
    }

}
