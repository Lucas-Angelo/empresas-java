package com.lucasangelo.imdb.services;

import java.util.List;
import java.util.Optional;

import com.lucasangelo.imdb.domain.Actor;
import com.lucasangelo.imdb.repositories.ActorRepository;
import com.lucasangelo.imdb.services.exceptions.DataIntegrityException;
import com.lucasangelo.imdb.services.exceptions.ObjectNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class ActorService {
    
    @Autowired
    private ActorRepository repository;

    public Actor find(Integer id) {
        Optional<Actor> obj = repository.findById(id); 
        return obj.orElseThrow(() -> new ObjectNotFoundException(
            "Objeto não encontrado! Id: " + id + ", Tipo: " + Actor.class.getName())
        ); 
    }

    public Actor insert(Actor obj) {
        obj.setId(null);
        return repository.save(obj);
    }

    public Actor update(Actor obj) {
        find(obj.getId());
        return repository.save(obj);
    }

    public void deleteById(Integer id) {
        find(id);
        try {
            repository.deleteById(id);
        } catch (DataIntegrityViolationException e) {
            throw new DataIntegrityException("Não é possível excluir este gênero");
        }
    }

    public List<Actor> findAll() {
        return repository.findAll();
    }

}
