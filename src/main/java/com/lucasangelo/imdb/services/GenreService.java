package com.lucasangelo.imdb.services;

import java.util.List;
import java.util.Optional;

import com.lucasangelo.imdb.domain.Genre;
import com.lucasangelo.imdb.repositories.GenreRepository;
import com.lucasangelo.imdb.services.exceptions.DataIntegrityException;
import com.lucasangelo.imdb.services.exceptions.ObjectNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class GenreService {
    
    @Autowired
    private GenreRepository repository;

    public Genre find(Integer id) {
        Optional<Genre> obj = repository.findById(id); 
        return obj.orElseThrow(() -> new ObjectNotFoundException(
            "Objeto não encontrado! Id: " + id + ", Tipo: " + Genre.class.getName())
        ); 
    }

    public Genre insert(Genre obj) {
        obj.setId(null);
        return repository.save(obj);
    }

    public Genre update(Genre obj) {
        find(obj.getId());
        return repository.save(obj);
    }

    public void deleteById(Integer id) {
        find(id);
        try {
            repository.deleteById(id);
        } catch (DataIntegrityViolationException e) {
            throw new DataIntegrityException("Não é possível excluir este gênero");
        }
    }

    public List<Genre> findAll() {
        return repository.findAll();
    }

}
