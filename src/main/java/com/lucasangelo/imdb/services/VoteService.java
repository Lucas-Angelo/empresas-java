package com.lucasangelo.imdb.services;

import java.util.List;
import java.util.Optional;

import com.lucasangelo.imdb.domain.Movie;
import com.lucasangelo.imdb.domain.User;
import com.lucasangelo.imdb.domain.Vote;
import com.lucasangelo.imdb.domain.enums.Profile;
import com.lucasangelo.imdb.dto.VoteNewDTO;
import com.lucasangelo.imdb.repositories.MovieRepository;
import com.lucasangelo.imdb.repositories.UserRepository;
import com.lucasangelo.imdb.repositories.VoteRepository;
import com.lucasangelo.imdb.security.UserSS;
import com.lucasangelo.imdb.services.exceptions.AuthorizationException;
import com.lucasangelo.imdb.services.exceptions.DataIntegrityException;
import com.lucasangelo.imdb.services.exceptions.ObjectNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class VoteService {

    @Autowired
    private VoteRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MovieRepository movieRepository;

    public Vote create(Vote obj) {
        UserSS user = UserService.authenticated();

        if(user==null || !user.hasRole(Profile.ADMIN) && !obj.getId().equals(user.getId()))
            throw new AuthorizationException("Acesso negado");
        
        obj.setId(null); // To protect if Id isn't null...
        return repository.save(obj);
    }

    public void delete(Integer id){
        find(id);
        try {
            repository.deleteById(id);
        } catch (DataIntegrityException e) {
            throw new DataIntegrityException("Não é possível excluir este usuário.");
        }
    }

    public Vote find(Integer id) {
        Optional<Vote> obj = repository.findById(id); 
        return obj.orElseThrow(() -> new ObjectNotFoundException(
            "Objeto não encontrado! Id: " + id + ", Tipo: " + Vote.class.getName())
        ); 
    }

    public List<Vote> findAll() {
        return repository.findAll();
    }

    public Vote fromDTO(VoteNewDTO objDto) {
        Optional<User> user = userRepository.findById(objDto.getUser());
        Optional<Movie> movie = movieRepository.findById(objDto.getMovie());
        return new Vote(null, objDto.getRate(), user.get(), movie.get());
    }

	public Page<Vote> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repository.
        findAll(pageRequest);
	}

}
