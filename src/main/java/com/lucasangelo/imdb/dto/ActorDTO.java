package com.lucasangelo.imdb.dto;

import java.io.Serializable;

import com.lucasangelo.imdb.domain.Actor;

public class ActorDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    
    public ActorDTO() {
    }

    public ActorDTO(Actor obj) {
        this.id = obj.getId();
        this.name = obj.getName();
    }

    public ActorDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
