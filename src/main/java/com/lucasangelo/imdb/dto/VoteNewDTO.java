package com.lucasangelo.imdb.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

public class VoteNewDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "Preenchimento obrigatório")
    private Double rate;
    
    @NotEmpty(message = "Preenchimento obrigatório")
    private Integer user;

    @NotEmpty(message = "Preenchimento obrigatório")
    private Integer movie;

    public VoteNewDTO() {
    }

    public VoteNewDTO(Double rate, Integer user, Integer movie) {
        this.rate = rate;
        this.user = user;
        this.movie = movie;
    }

    public Double getRate() {
        return this.rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Integer getUser() {
        return this.user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Integer getMovie() {
        return this.movie;
    }

    public void setMovie(Integer movie) {
        this.movie = movie;
    }

}
