package com.lucasangelo.imdb.dto;

import java.io.Serializable;

import com.lucasangelo.imdb.domain.User;
import com.lucasangelo.imdb.domain.Vote;

public class VoteDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Double rate;
    private String movie;
    private User user;
    
    public VoteDTO() {
    }

    public VoteDTO(Vote obj) {
        this.id = obj.getId();
        this.rate = obj.getRate();
        this.movie = obj.getMovie().getName();
        this.user = obj.getUser();
    }

    public VoteDTO(Integer id, Double rate, String movie, User user) {
        this.id = id;
        this.rate = rate;
        this.movie = movie;
        this.user = user;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getRate() {
        return this.rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getMovie() {
        return this.movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    

}
