package com.lucasangelo.imdb.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.lucasangelo.imdb.domain.User;
import com.lucasangelo.imdb.domain.enums.Profile;
import com.lucasangelo.imdb.services.validation.UserUpdate;

@UserUpdate // @Valid user only updates their own email
public class UserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(access = Access.READ_ONLY)
    private Integer id;

    @Size(max = 255, min = 1, message = "O tamanho deve ser entre 1 e 255 caracteres")
    @NotEmpty(message = "Preenchimento obrigatório")
    private String name;

    @Size(max = 255, min = 1, message = "O tamanho deve ser entre 1 e 255 caracteres")
    @NotEmpty(message = "Preenchimento obrigatório")
    @Email(message = "Deve ser um endereço de e-mail bem formado")
    private String email;

    @Size(max = 255, min = 1, message = "O tamanho deve ser entre 1 e 255 caracteres")
    @NotEmpty(message = "Preenchimento obrigatório")
    @JsonProperty(access = Access.WRITE_ONLY) // Not to show this field in response
    private String password;
    
    private Set<Profile> profiles = new HashSet<>();

    public UserDTO() {
    }

    public UserDTO(Integer id, String name, String email, String password, Set<Profile> profiles) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.profiles = profiles;
    }

    public UserDTO(User obj) {
        this.id = obj.getId();
        this.name = obj.getName();
        this.email = obj.getEmail();
        this.profiles = obj.getProfiles();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Profile> getProfiles() {
        return this.profiles;
    }

    public void setProfiles(Set<Profile> profiles) {
        this.profiles = profiles;
    }

}
