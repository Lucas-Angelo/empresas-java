package com.lucasangelo.imdb.dto;

import java.io.Serializable;

import com.lucasangelo.imdb.domain.Genre;

public class GenreDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    
    public GenreDTO() {
    }

    public GenreDTO(Genre obj) {
        this.id = obj.getId();
        this.name = obj.getDescription();
    }

    public GenreDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
