package com.lucasangelo.imdb.dto;

import java.io.Serializable;

public class CredentialsDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String email; // Username
    private String password;

    public CredentialsDTO() {
    }

    public CredentialsDTO(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
