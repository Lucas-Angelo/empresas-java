package com.lucasangelo.imdb.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.lucasangelo.imdb.services.validation.UserCreate;

@UserCreate // @Valid simple check existent email
public class UserNewDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Size(max = 255, min = 1, message = "O tamanho deve ser entre 1 e 255 caracteres")
    @NotEmpty(message = "Preenchimento obrigatório")
    private String name;

    @Size(max = 255, min = 1, message = "O tamanho deve ser entre 1 e 255 caracteres")
    @NotEmpty(message = "Preenchimento obrigatório")
    @Email(message = "Deve ser um endereço de e-mail bem formado")
    private String email;

    @Size(max = 255, min = 1, message = "O tamanho deve ser entre 1 e 255 caracteres")
    @NotEmpty(message = "Preenchimento obrigatório")
    @JsonProperty(access = Access.WRITE_ONLY) // Not to show this field in response
    private String password;

    public UserNewDTO() {
    }

    public UserNewDTO(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
