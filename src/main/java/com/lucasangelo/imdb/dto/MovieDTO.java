package com.lucasangelo.imdb.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.lucasangelo.imdb.domain.Actor;
import com.lucasangelo.imdb.domain.Genre;
import com.lucasangelo.imdb.domain.Movie;
import com.lucasangelo.imdb.domain.Vote;

public class MovieDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private String director;
    private List<Genre> genres = new ArrayList<>();
    private List<Actor> actors = new ArrayList<>();
    private List<Vote> votes = new LinkedList<>();
	
	public MovieDTO() {
	}

    public MovieDTO(Integer id, String name, String director, List<Genre> genres, List<Actor> actors, List<Vote> votes) {
        this.id = id;
		this.name = name;
		this.director = director;
		this.genres = genres;
		this.actors = actors;
		this.votes = votes;
    }

	public MovieDTO(Movie obj) {
		this.id = obj.getId();
		this.name = obj.getName();
		this.director = obj.getDirector();
		this.genres = obj.getGenres();
		this.actors = obj.getActors();
		this.votes = obj.getVotes();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDirector() {
		return this.director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public List<Genre> getGenres() {
		return this.genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public List<Actor> getActors() {
		return this.actors;
	}

	public void setActors(List<Actor> actors) {
		this.actors = actors;
	}

	public List<Vote> getVotes() {
		return this.votes;
	}

	public void setVotes(List<Vote> votes) {
		this.votes = votes;
	}

    public Double getRating() {
        if (this.votes.isEmpty())
            return 0.0d;
        else {
            Double sum = 0.0d;
            for (Vote vote : this.votes) {
                sum += vote.getRate();
            }
            return sum/this.votes.size();
        }
    }

}
