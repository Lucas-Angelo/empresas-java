package com.lucasangelo.imdb.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

public class MovieNewDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "Preenchimento obrigatório")
    private String name;
    
    @NotEmpty(message = "Preenchimento obrigatório")
    private String director;

    @NotEmpty(message = "Preenchimento obrigatório")
    private String genres;

    @NotEmpty(message = "Preenchimento obrigatório")
    private String actors;

    public MovieNewDTO() {
    }

    public MovieNewDTO(String name, String director, String genres, String actors) {
        this.name = name;
        this.director = director;
        this.genres = genres;
        this.actors = actors;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirector() {
        return this.director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getGenres() {
        return this.genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getActors() {
        return this.actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

}
