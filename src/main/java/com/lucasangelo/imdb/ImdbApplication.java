package com.lucasangelo.imdb;

import java.util.Arrays;

import com.lucasangelo.imdb.domain.Actor;
import com.lucasangelo.imdb.domain.Genre;
import com.lucasangelo.imdb.domain.Movie;
import com.lucasangelo.imdb.domain.User;
import com.lucasangelo.imdb.domain.Vote;
import com.lucasangelo.imdb.domain.enums.Profile;
import com.lucasangelo.imdb.repositories.ActorRepository;
import com.lucasangelo.imdb.repositories.GenreRepository;
import com.lucasangelo.imdb.repositories.MovieRepository;
import com.lucasangelo.imdb.repositories.UserRepository;
import com.lucasangelo.imdb.repositories.VoteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class ImdbApplication implements CommandLineRunner {
	
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private GenreRepository genreRepository;
	@Autowired
	private ActorRepository actorRepository;
	@Autowired
	private VoteRepository voteRepository;
	@Autowired
	private MovieRepository movieRepository;

	public static void main(String[] args) {
		SpringApplication.run(ImdbApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		User user0 = new User(null, "Lucas", "lcs2001_@hotmail.com", bCryptPasswordEncoder.encode("supersenha"));
		user0.addProfile(Profile.ADMIN); // System need admins.
		User user1 = new User(null, "Ana", "ana@email.com", bCryptPasswordEncoder.encode("supersenha"));
		user1.addProfile(Profile.ADMIN); // Why not other admin? :)
		User user2 = new User(null, "Luisa", "luisa@email.com", bCryptPasswordEncoder.encode("supersenha"));
		User user3 = new User(null, "Pedro", "pedro@email.com", bCryptPasswordEncoder.encode("supersenha"));
		User user4 = new User(null, "Julia", "julia@email.com", bCryptPasswordEncoder.encode("supersenha"));
		User user5 = new User(null, "Joao", "joao@email.com", bCryptPasswordEncoder.encode("supersenha"));
		User user6 = new User(null, "Laura", "laura@email.com", bCryptPasswordEncoder.encode("supersenha"));
		User user7 = new User(null, "Jose", "jose@email.com", bCryptPasswordEncoder.encode("supersenha"));
		this.userRepository.saveAll(Arrays.asList(user0, user1, user2, user3, user4, user5, user6, user7));

		Genre genre0 = new Genre(null, "Biografia");
		Genre genre1 = new Genre(null, "Drama");
		Genre genre2 = new Genre(null, "Aventura");

		Actor actor0 = new Actor(null, "Benedict Cumberbatch");
		Actor actor1 = new Actor(null, "Denzel Washington");

		Movie movie0 = new Movie(null, "The Imitation Game", "Morten Tyldum", Arrays.asList(genre0, genre1), Arrays.asList(actor0));
		Movie movie1 = new Movie(null, "The Magnificent Seven", "Antoine Fuqua", Arrays.asList(genre2), Arrays.asList(actor1));

		genre0.getMovies().addAll(Arrays.asList(movie0));
		genre1.getMovies().addAll(Arrays.asList(movie0));
		genre2.getMovies().addAll(Arrays.asList(movie1));

		actor0.getMovies().addAll(Arrays.asList(movie0));
		actor1.getMovies().addAll(Arrays.asList(movie1));

		Vote vote0 = new Vote(null, 4.0, user2, movie0);
		Vote vote1 = new Vote(null, 0.0, user3, movie0);
		Vote vote2 = new Vote(null, 4.0, user2, movie1);

		movie0.getVotes().addAll(Arrays.asList(vote0, vote1));
		movie1.getVotes().addAll(Arrays.asList(vote2));

		genreRepository.saveAll(Arrays.asList(genre0, genre1, genre2));
		actorRepository.saveAll(Arrays.asList(actor0, actor1));
		movieRepository.saveAll(Arrays.asList(movie0, movie1));
		voteRepository.saveAll(Arrays.asList(vote0, vote1, vote2));

	}

}
